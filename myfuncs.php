<?php
// Project Name: Milestone7
// Project Version: 1.6
// Module Name: Search Blog Module
// Module Version: 1.6
// Programmer Name: Justin Gewecke
// Date: 8/2/2020
// Description: This module handles searching portion of the blog website
// References: https://www.w3schools.com/php/php_mysql_insert.asp

function dbConnect() {
    // Connect to azure
    $link = mysqli_connect("127.0.0.1", "azure", "6#vWHD_$", "localdb", "52757");
    
    // Connect to local
    //$link = mysqli_connect("127.0.0.1", root, root, "activity1");
    
    // Check connection
    if($link === false){
        die("ERROR (myfuncs.php): Could not connect. " . mysqli_connect_error());
    }
    return $link;
}

function saveUserId($id)
{
    session_start();
    $_SESSION["USER_ID"] = $id;
    $_SESSION["POST_ID"] = 0;
}
function getUserId()
{
    session_start();
    return $_SESSION["USER_ID"];
}

function forgetUserId()
{
    session_start();
    $_SESSION = array();
}

// Uses session to automatically get userID
function getUserArrayFromCurrentUser($link)
{
    session_start();
    $userID = $_SESSION["USER_ID"];
    
    $sql = "SELECT * FROM users WHERE ID='$userID'";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);
    if ($numRows >= 1)
    {
        return $result->fetch_assoc();	// Read the Row from the Query
    }
    else
    {
        return null;
    }
}

function getPostFromID($link, $id)
{   
    $sql = "SELECT * FROM posts WHERE ID='$id'";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);
    if ($numRows >= 1)
    {
        return $result->fetch_assoc();	// Read the Row from the Query
    }
    else
    {
        echo('(myfuncs.php) No post found in database');
        return null;
    }
}

function getUsersByFirstName($link, $pattern)
{
    $sql = "SELECT * FROM users WHERE FIRST_NAME LIKE '$pattern'";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);
    if (mysqli_num_rows($result) > 0) {
        $index = 0;
        while ($row = mysqli_fetch_assoc($result)) {
            $users[$index] = array($row["ID"], $row["FIRST_NAME"], $row["LAST_NAME"]);

            ++$index;
        }

        return $users;
    }
    else {
        return null;
    }
}

function getAllUsers($link)
{
    // Check connection
    if($link === false){
        die("ERROR: Could not connect. " . mysqli_connect_error());
    }

    $sql = "SELECT FIRST_NAME, LAST_NAME FROM users";
    $result = mysqli_query($link, $sql);

    if (mysqli_num_rows($result) > 0) {
        $index = 0;
        while ($row = mysqli_fetch_assoc($result)) {
            $users[$index] = array($row["ID"], $row["FIRST_NAME"], $row["LAST_NAME"]);

            ++$index;
        }

        return $users;
    }
    else {
        return null;
    }
}

function search_request($link, $sql)
{

    $result = mysqli_query($link, $sql);


    $numRows = mysqli_num_rows($result);

    if ($numRows >= 1)
    {
        foreach($result as $i) {
            $sql = "SELECT ID, AUTHOR_ID, TITLE, TEXT FROM posts"; 
            
            $postID = $i["ID"];
            $text = $i["TEXT"];
            $title = $i["TITLE"];
            $authorID = $i["AUTHOR_ID"];
            $tag = $i["TAG"];
    
            // Get First and Last name of Author from AUTHOR_ID
            $sql = "SELECT ID, FIRST_NAME, LAST_NAME, EMAIL, USERNAME, PASSWORD FROM users WHERE ID='$authorID'";
            $i = mysqli_query($link, $sql);
            $row = $i->fetch_assoc();	// Read the Row from the Query
            $author = $row["FIRST_NAME"] . ' ' . $row["LAST_NAME"];
            
            // Display result
            echo nl2br("<h1>$title</h1> <h2>by $author</h2> <p>$text</p> <p id='tag'>Tag: $tag</p>");
    
            // Allow moderators and higher to edit/delete ANY post
            if (getUserArrayFromCurrentUser($link)["PERMISSION_LEVEL"] >= 1)
            {
                
                echo('<form action="modPanel.php" method="POST">
                         <input type="hidden" name="Data" value="'.$postID.'"/>
                         <input type="submit" value="Edit in Mod Panel" />
                      </form>');
            }
            // Allow user to delete/edit their own post; we can just re-use the mod panel for this
            else if ($authorID == getUserId())
            {
                echo('<form action="modPanel.php" method="POST">
                         <input type="hidden" name="Data" value="'.$postID.'"/>
                         <input type="submit" value="Edit Post" />
                      </form>');
            }
        }
    }

    return $numRows;
}

?>