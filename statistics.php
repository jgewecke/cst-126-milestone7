<?php
// Project Name: Milestone7
// Project Version: 1.6
// Module Name: Search Blog Module
// Module Version: 1.6
// Programmer Name: Justin Gewecke
// Date: 8/2/2020
// Description: This module handles searching portion of the blog website
// References: https://www.w3schools.com/php/php_mysql_insert.asp

require_once('myfuncs.php');

function getNumberOfUsers($link)
{
    $sql = "SELECT ID FROM users";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);

    return $numRows;
}

function getNumberOfPosts($link)
{
    $sql = "SELECT ID FROM posts";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);

    return $numRows;
}

function getNumberOfGuests($link)
{
    $sql = "SELECT PERMISSION_LEVEL FROM users WHERE PERMISSION_LEVEL=0";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);

    return $numRows;
}

function getNumberOfModerators($link)
{
    $sql = "SELECT PERMISSION_LEVEL FROM users WHERE PERMISSION_LEVEL=1";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);

    return $numRows;
}

function getNumberOfAdmins($link)
{
    $sql = "SELECT PERMISSION_LEVEL FROM users WHERE PERMISSION_LEVEL=2";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);

    return $numRows;
}

?>