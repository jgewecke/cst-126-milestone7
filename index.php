<!-- 
// Project Name: Milestone7
// Project Version: 1.6
// Module Name: Search Blog Module
// Module Version: 1.6
// Programmer Name: Justin Gewecke
// Date: 8/2/2020
// Description: This module handles searching portion of the blog website

// TODO: Prevent Empty Tags
-->

<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>My Milestone 7 Application</title>
	</head>
	<body>
		<h2>My Milestone 7 Application</h2>
		<h3>Please login to make a post</h3>

		<div class="group">
			<a href="post.php">Create a Post</a>
		</div>
		<div class="group">
			<a href="viewPosts.php">View All Posts</a>
		</div>
		<div class="group">
			<a href="search.php">Search for Post</a>
		</div>
	</body>
</html>

<?php
	require_once('myfuncs.php');

	$link = dbConnect();


	echo ('<div class="group">');
			

	if (getUserId() == null)
	{
		echo('<a href="login.html">Login</a> ');
	}
	else
	{
		echo('<a href="logoutHandler.php">Sign Out</a> ');
	}
	echo ('<a href="register.html">Register</a></div>');

	// Allow Admins to see Admin panel
	if (getUserArrayFromCurrentUser($link)["PERMISSION_LEVEL"] == 2)
	{
		
		echo('<div class="group">
				<a href="adminPanel.php">Admin Panel</a>
			</div>');
	}
?>