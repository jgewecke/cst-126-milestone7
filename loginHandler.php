<p><a href="index.php">Click here to return to main menu.</a></p>
<p><a href="post.php">Click here to make a post.</a></p>


<?php
// Project Name: Milestone7
// Project Version: 1.6
// Module Name: Search Blog Module
// Module Version: 1.6
// Programmer Name: Justin Gewecke
// Date: 8/2/2020
// Description: This module handles searching portion of the blog website

/* ---User Authentification---
 * Username and password <= 50 characters
 * Unlimited attempts allowed
 * Certain characters are not allowed: ' " / \ [ ] ( ) { }
 * Password is hidden when typed
 * Username is case-insensitive
 * Password is case-sensitive !--This is done within phpMyAdmin by settings the PASSWORD column collation to latin1_general_cs--!
 */

require_once('myfuncs.php');

$link = dbConnect();

// Input
$username = $_POST['Username'];
$password = $_POST['Password'];

// Check for empty input, otherwise exit out with an error
if ($username == NULL)  { echo "The username is a required field and cannot be blank.\n";           return; }
if ($password == NULL)  { echo "The password is a required field and cannot be blank.\n";           return; }

// Sanitizing input in case of sql injection
$username = str_replace("'", "", $username);
$password= str_replace("'", "", $password);

$sql = "SELECT ID, FIRST_NAME, LAST_NAME, EMAIL, USERNAME, PASSWORD FROM users WHERE USERNAME='$username' AND PASSWORD='$password'";
$result = mysqli_query($link, $sql);
$numRows = mysqli_num_rows($result);

// Check for matches in our database
// We have too many of this user
if ($numRows > 2) {
    $message = "There are multiple users registered with that information";
    include('./loginFailed.php');
}
else if ($numRows == 1) {
    $row = $result->fetch_assoc();	// Read the Row from the Query
    saveUserId($row["ID"]);		// Save User ID in the Session
    include('./loginResponse.php');
}
else if ($numRows == 0) {
    $message = "Login Failed";
    include('./loginFailed.php');
}
else {
    $message = "ERROR: Could not able to execute $sql. " . mysqli_error($link);
    include('./loginFailed.php');
}

// Close connection
mysqli_close($link);
?>